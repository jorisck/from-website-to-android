import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(private inAppBrowser : InAppBrowser, private platform : Platform) {}

  subscription: any

  ngOnInit(){
   
  }

  launchSite(){
    this.inAppBrowser.create(`https://koalift.com`, `_blank`,'location=no,toolbar=no,zoom=no');
  }

  exitApp(){
    navigator['app'].exitApp();
  }

  ionViewDidEnter(){
    this.subscription = this.platform.backButton.subscribe(async () => {
        navigator['app'].exitApp();
  });
  }
  
  ionViewWillLeave(){
    this.subscription.unsubscribe();
  }

}
