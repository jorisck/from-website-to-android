# Convert a website to an Android app with the Ionic framework. 

## Table of Contents
- [Getting Started](#getting-started)
- [Working environment](#working-environment)
- [Commands used to build this projet](#commands-used-to-build-this-projet)
  - [Tutorial](#tutorial)
  - [Commands](#commands)
- [Bugs](#bugs)
- [Usefull links cross-platform full-stack developing](#usefull-links-cross-platform-full-stack-developing)
- [Sources](#sources)
- [More](#more)


## Getting Started
* [Download the installer](https://nodejs.org/en/) for Node LTS.
* Install the ionic CLI globally: `npm install -g ionic@4.12.0`
* Clone this repository: `git clone git clone git@gitlab.com:jorisck/from-website-to-android.git`
* Run `npm install` from the project root.
* Run `ionic serve` in a terminal from the project root.

## Working environment
- Ionic framework: @ionic/angular 6.0.4       
- Ionic CLI: v4.12.0
- Node.js : v12.20.0
- Cordova CLI: v10.0.0      
- Android SDK Tools: 26.1.1    
- OS: Windows 7  


### Soft
- AndroidStudio 


## Commands used to build this projet

### Tutorial (in French)

[How to quickly convert a web application into an Android app for free?](https://erablog.substack.com/p/comment-passer-rapidement-et-gratuitement)

### Commands

`mkdir ionic42`
  
`cd ionic42`

`npm install -g ionic@4.12.0`

`ionic start testapp tabs` 
> [more themes here](https://ionicframework.com/docs/v3/cli/starters.html)

`cd testapp`

`npm install @ionic-native/core --save`

`ionic cordova plugin add cordova-plugin-inappbrowser`

`npm install @ionic-native/in-app-browser`
	
`ionic serve`

> Go to config.xml (root dir) and update id (and other information) before adding your platform to avoir [this issues](https://stackoverflow.com/questions/20266951/no-java-files-found-which-extend-cordovaactivity-when-use-cordova-build)

`ionic cordova platform add android@10.0.0`
> to deploy on Play store need API Level 29

`ionic cordova build android --info`

## Bugs

* error with jvm 32 in gradle ==> use 64-bits jdk. [See for more](https://stackoverflow.com/questions/42754758/failure-build-failed-with-an-exception-ionic)

* android sdk packages-licences have not been accepted error. [See for more](https://stackoverflow.com/questions/54273412/failed-to-install-the-following-android-sdk-packages-as-some-licences-have-not)

* if concern with Gradle although AS is installed:
[Click here for Gradle installation and how add it in your path](https://www.freakyjolly.com/resolve-could-not-find-an-installed-version-of-gradle-either-in-android-studio/)

## Usefull links cross-platform full-stack developing
https://auth0.com/blog/converting-your-web-app-to-mobile/

https://forum.ionicframework.com/t/convert-website-into-android-ios-app/129312

## Sources:
* Tutorial from [here](https://techonda.medium.com/how-to-convert-website-into-an-app-for-free-with-ionic-4-9201a6f993dc)

* [Github code](https://github.com/bamboriz/web2app)

## More

* A video of [the result](https://www.linkedin.com/posts/joris-k-85246510a_what-ton-client-veut-pour-un-m%C3%AAme-produit-activity-6894207580827115522--IGi)

* Another ionic github project: [Ionic Angular Conference Application](https://github.com/ionic-team/ionic-conference-app)